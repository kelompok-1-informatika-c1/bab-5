// Soal Bab 5 Nomor 2
// Translasilah algoritma pada latihan Bab 4 ke dalam program pascal, C, C++

#include <iostream>
using namespace std;

main() {
    cout <<"--------------------------------------------------------------\n";
    cout << "\t		   BAB 5  \n";
    cout << "\t		SOAL NOMOR 2\n";
    cout <<"--------------------------------------------------------------\n";
    cout <<"=============== MENGHITUNG LUAS SEGITIGA =====================\n";
    float a, t, luas;
    cout << "Masukkan alas segitiga (cm) : ";
    cin >> a;
    cout << "Masukkan tinggi segitiga (cm) : ";
    cin >> t;

    luas = 0.5*a*t;
    cout << "~~Hasil perhitungan luas segitiga~~ \n";
    cout << "==>> Luas segitiga : 1/2 x alas x tinggi \n";
    cout << "==>> Luas segitiga : " << "1/2 x " << a << " x " << t << endl;
    cout << "==>> Luas segitiga : " << luas << " cm^2 \n"; 
}
