// Bab 5 Nomor 5
// Tulislah program Pascal/C/C++ untuk menghitung gaji PNS yang
// terdiri dari gaji pokok, tunjangan tunjangan anak, dan tunjangan istri

#include <iostream>
using namespace std;

main() {
    cout << "Bab 5 Nomor 5\n";
    cout << "==============================================\n";
    cout<<"\t PROGRAM MENGHITUNG GAJI PNS\n";
    cout << "==============================================\n";
    
    unsigned long int pokok, istri, anak, t_anak, total;
    cout << "Masukkan gaji pokok anda : ";cin >> pokok;
    
    cout << "Jumlah anak\t\t : ";cin >> anak;
    
    istri = 0.1*pokok;
    t_anak = 0.05*pokok*anak;
    
    total = pokok + istri + t_anak;
 
    cout << "==============================================\n";   
    cout << "BERIKUT ADALAH JUMLAH GAJI YANG ANDA TERIMA \n";
    cout << "==============================================\n";
    cout << "Gaji Pokok\t\t\t = Rp" << pokok << endl;
    cout << "Tunjangan Istri 10%\t\t = Rp" << istri << endl;
    cout << "Tunjangan 5% " << anak << " anak\t\t = Rp" << t_anak << endl;
    cout << "Total gaji yang diperoleh\t = Rp" << total << endl; 
}
